var promise = require('promise');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/chat');

var dataBase = mongoose.connection;

dataBase.on('error', function(err) {
    console.log('DB - error: ' + err.message);
});

dataBase.once('open', function() {
    console.log('Connected to MongoDB!');
});

var User = new mongoose.Schema({
    _id: {type: String, required: true},
    user_name: {type: String, required: true},
    logo_url: {type: String, required: true},
    room_id: []
});
var UserModel = mongoose.model('User', User, 'User');

var Message = new mongoose.Schema({
    date_time: {type: Date, required: true},
    user_id: {type: String, required: true, ref: 'User'},
    room_id: {type: String, required: true, ref: 'Room'},
    message: {type: String, required: true}
});
var MessageModel = mongoose.model('Message', Message, 'Message');

var Room = new mongoose.Schema({
    //_id: {type: mongoose.Schema.Types.ObjectId, required: true},
    room_name: {type: String, required: true},
    is_default: {type: Boolean, required: true},
    owner_id: {type: String, required: true, ref: 'User'},
    invitation: [] // user_id (cid)
});
var RoomModel = mongoose.model('Room', Room, 'Room');

module.exports.createUser = function(userData, cb) {
    var userModel = UserModel({
        _id: userData.cid,
        user_name: userData.sender,
        logo_url: userData.logoUrl,
        room_id: []
    });
    userModel.save(function(err, user) {
        if(err) {
            socket.emit('error', {message: 'DB-error on save user'});
            return;
        }
        cb(user);
    });
};

var updateUser = function(whereObj, updateObj, optionsObj, cb) {
    UserModel.update(whereObj, updateObj, optionsObj, function(err, affected, result) {
        if(err) {
            cb(err, null, null);
        }
        cb(err, affected, result);
    });
};

module.exports.updateUser = updateUser;

function dbFieldToCamelCase(object) {
    var newObject = {};
    for(var key in object) {
        var keyName = '';
        var pieces = key.split(/_/);
        for (var i = 0; i < pieces.length; i++) {
            if (pieces[i] !== '') {
                if (keyName === '') {
                    keyName += pieces[i];
                } else {
                    keyName += pieces[i].charAt(0).toUpperCase() + pieces[i].substr(1);
                }
            }
        }
        newObject[keyName] = object[key];
    }
    return newObject;
}

function getUserById(userId, cb) {
    UserModel.findById(userId, function(err, user) {
        if (err) {
            socket.emit('error', {message: 'DB-error on get user'});
            cb(false);
        }
        cb(user);
    });
}

module.exports.getUserById = getUserById;

var getUserByName = function(package) {
    var searchRegExp = new RegExp(package.searchPart, 'i');
    return new promise(function(fulfill, reject) {
        var ObjectId = mongoose.Types.ObjectId;
        var roomId = new ObjectId(package.roomId);
        UserModel.find({$and: [{user_name: searchRegExp}, {_id: {$ne: package.cid}}, {room_id: {$nin: [roomId]}}] })
            .select('_id user_name logo_url').limit(15).exec(function(err, result) {
                if(err) {
                    console.log('Get rooms error:' + err.message);
                    reject(err);
                }else {
                    var users = [];
                    if (result.length) {
                        Array.prototype.forEach.call(result, function (item) {
                            users.push({cid: item._id, userName: item.user_name, logoUrl: item.logo_url});
                        });
                    }
                    fulfill(users);
                }
            });
    });
    /*
    * Might need to use search by website URL
    * {$or :
     [
     {$and: [{user_name: searchRegExp}, {cid: {$not: package.cid}}]},
     {$and: [{user_name: searchRegExp}, {cid: {$not: package.cid}}]}
     ]}
    * */
};

module.exports.getUserByName = getUserByName;

var getRooms = function (user, cb) {
    /*
    * Get list of all rooms current user is related to
    * user.room_id - is an array of room IDs
    * */
    RoomModel.find({$or: [ {is_default: true}, {_id: {$in: user.room_id}}, {owner_id: user._id} ]}, function (err, result) {
        if (err) {
            console.log('Get rooms error:' + err.message);
            return false;
        }
        var rooms = [];
        if (result.length) {
            for(var i = 0; i < result.length; i++) {
                rooms.push({
                    id: result[i]._id,
                    name: result[i].room_name,
                    isDefault: result[i].is_default,
                    ownerId: result[i].owner_id,
                    invitation: result[i].invitation
                });
            }
        }
        cb(rooms);
    });
};

module.exports.getRooms = getRooms;

module.exports.createDefaultRooms = function(rooms) {
    Array.prototype.forEach.call(rooms, function(val, i) {
        RoomModel.findOne({$and: [{'room_name': rooms[i]}, {is_default: true}]}, function (err, result) {
            if (!result) {
                var roomModel = new RoomModel({
                    room_name: rooms[i],
                    is_default: true,
                    owner_id: '0'
                });
                roomModel.save(function (err) {
                    if (err) {
                        console.log('Create default error' + err.message);
                        return false;
                    }
                });
            }
        });
    });
};

module.exports.createRoom = function(io, socket, connectionMap, package) {
    var room = new RoomModel({
        room_name: package.roomName,
        is_default: false,
        owner_id: package.cid,
        invitation: []
    });
    room.save(function (err, newRoom) {
        if (err) {
            console.log('Create room error' + err.message);
            return false;
        } else {
            socket.join(newRoom._id);
            getUserById(package.cid, function(user) {
                getRooms(user, function(rooms) {
                    socket.emit('room', {rooms: rooms});
                });
            });
        }
    });
};

module.exports.deleteRoom = function(io, socket, connectionMap, package) {
    // If user leaves the room
    var ObjectId = mongoose.Types.ObjectId;
    var roomId = new ObjectId(package.id);

    if(package.cid !== package.ownerId) {
        updateUser({_id: package.cid}, {$pull: {room_id: roomId}}, {multi: false}, function (err, affected) {
            if (err) {
                socket.emit('error', {message: 'DB-error on leave room'});
                return false;
            }
            socket.leave(package.id);
            getUserById(package.cid, function(user) {
                getRooms(user, function (rooms) {
                    socket.emit('room', {rooms: rooms});
                    getRoomUsers(io, socket, null, {roomId: package.id});
                });
            });
        });
    }else if(package.cid === package.ownerId) { // If user removes it's own room
        RoomModel.find({_id: roomId}, {_id: 0, invitation: 1}, function(err, result) {
            if (err) {
                console.log('Delete room error' + err.message);
                return false;
            }

            var invitations = result.pop().invitation;
            // Get users accepted invite
            UserModel.find({room_id: {$in: [roomId]}}, {_id: 1}, function(err, userIds) {
                userIds = userIds.map(function(item) {
                    return item.id;
                });
                userIds.push(package.cid);
                var roomUserIds = invitations.concat(userIds); // List of user IDs to inform about deleted room + room owner ID
                RoomModel.findOneAndRemove({_id: package.id, is_default: false, owner_id: package.cid}, function (err) {
                    if (err) {
                        console.log('Delete room error' + err.message);
                        return false;
                    }
                    updateUser({room_id: roomId}, {$pull: {room_id: roomId}}, {multi: true}, function (err, affected, result) {
                        if (err) {
                            socket.emit('error', {'message': 'Error user update on delete room'});
                            console.log('Delete room error' + err.message);
                            return false;
                        }

                        MessageModel.remove({room_id: package.id}, function (err) {
                            if (err) {
                                console.log('Delete room error' + err.message);
                                return false;
                            }

                            Array.prototype.forEach.call(roomUserIds, function(userId) {
                                Array.prototype.forEach.call(connectionMap.clientToSockets[userId], function (sock) {
                                    getUserById(userId, function (user) {
                                        getRooms(user, function (rooms) {
                                            io.to(sock).emit('room', {rooms: rooms});
                                        });
                                    });
                                    // Remove user invite and notify socket
                                    if(invitations.indexOf(userId) !== -1) {
                                        getUserInvite(socket, userId, function(invites) {
                                            Array.prototype.forEach.call(connectionMap.clientToSockets[userId], function(sock) {
                                                io.to(sock).emit('invite', invites);
                                            });
                                        });
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};

/*Returns invites for specific user with information about person who sent an invitation*/
var getUserInvite = function(socket, cid, callback) {
    RoomModel.find({invitation: {$in: [cid]}}).populate('owner_id', 'user_name logo_url').exec(function(err, roomModel) {
        if(err) {
            console.log('Get invite error' + err.message);
            socket.emit('error', {message: 'DB-error on get invite'});
            return false;
        }
        var invites = [];
        if(roomModel.length) {
            Array.prototype.forEach.call(roomModel, function(item) {
                invites.push({
                    id: item._id,
                    roomName: item.room_name,
                    cid: item.owner_id._id,
                    userName: item.owner_id.user_name,
                    logoUrl: item.owner_id.logo_url
                });
            });
        }
        callback(invites);
    });
};
module.exports.getUserInvite = getUserInvite;

module.exports.createInvite = function(io, socket, connectionMap, package) {
    RoomModel.findOneAndUpdate({_id: package.roomId}, {$addToSet: {invitation: package.userId}}, function(err, model) {
        if(err) {
            console.log('Add invite error' + err.message);
            socket.emit('error', {message: 'DB-error on invite user'});
            return false;
        }
        if(connectionMap.clientToSockets[package.userId]) {
            getUserInvite(socket, package.userId, function(invites) {
                Array.prototype.forEach.call(connectionMap.clientToSockets[package.userId], function(sock) {
                    io.to(sock).emit('invite', invites);
                });
            });
        }
    });
};

/*Returns a list of all users related to room*/
var getRoomUsers = function(io, socket, connectionMap, package) {
    var ObjectId = mongoose.Types.ObjectId;
    var roomId = new ObjectId(package.roomId);
    /*Get user IDs which didn't accepted invite yet*/
    RoomModel.find({_id: roomId}, {_id: 0, invitation: 1}, function(err, result) {
        if(err) {
            console.log('Find awaiting user IDs error' + err.message);
            return false;
        }

        var invitations = result.pop().invitation;
        /*Get whole user set*/
        UserModel.find({$or:[ {_id: {$in: invitations}}, {room_id: {$in: [roomId]}} ]}, function(err, result) {
            if(err) {
                console.log('Find in room users error' + err.message);
                return false;
            }

            var users = [];
            if(result.length) {
                Array.prototype.forEach.call(result, function(user) {
                    users.push({
                        id: user._id,
                        userName: user.user_name,
                        logoUrl: user.logo_url,
                        inviteStatus: (invitations.indexOf(user._id) === -1) ? 'A' : 'W' // check if user ID is not in invitation
                    });
                });
            }
            io.to(package.roomId).emit('roomUsers', {roomId: package.roomId, users: users});
        });
    });
};

module.exports.getRoomUsers = getRoomUsers;

// Removes an invitation if acceptor-user rejected it
var deleteInvite = function(io, socket, connectionMap, package) {
    RoomModel.findOneAndUpdate({_id: package.roomId}, {$pull: {invitation: package.cid}}, function(err) {
        if(err) {
            socket.emit('error', {message: 'DB-error on delete invite'});
            return false;
        }
        getUserInvite(socket, package.cid, function(invites) {
            socket.emit('invite', invites);
            getRoomUsers(io, socket, connectionMap, package);
        });
    });
};
module.exports.deleteInvite = deleteInvite;

module.exports.applyInvite = function(io, socket, connectionMap, package) {
    var ObjectId = mongoose.Types.ObjectId;
    var roomId = new ObjectId(package.roomId);
    updateUser({_id: package.cid}, {$addToSet: {room_id: roomId}}, {multi: false}, function (err, affected) {
        if(err) {
            socket.emit('error', {message: 'DB-error on apply invite'});
            return false;
        }
        deleteInvite(io, socket, connectionMap, package); // on delete refresh room users statuses
        socket.join(package.roomId);
        getUserById(package.cid, function(user) {
            getRooms(user, function(rooms) {
                socket.emit('room', {rooms: rooms});
            });
        });
    });
};

module.exports.deleteUserFromRoom = function(io, socket, connectionMap, package) {
    var ObjectId = mongoose.Types.ObjectId;
    var roomId = new ObjectId(package.roomId);

    if(package.inviteStatus === 'A') {
        updateUser({_id: package.userId}, {$pull: {room_id: roomId}}, {multi: false}, function (err, affected) {
            if (err) {
                socket.emit('error', {message: 'DB-error on remove user from room'});
                return false;
            }
            getUserById(package.userId, function(user) {
                getRooms(user, function (rooms) {
                    Array.prototype.forEach.call(connectionMap.clientToSockets[package.userId], function(sock) {
                        io.sockets.connected[sock].leave(package.roomId);
                        io.to(sock).emit('room', {rooms: rooms});
                    });
                    getRoomUsers(io, socket, connectionMap, package);
                });
            });
        });
    }else if(package.inviteStatus === 'W') {
        RoomModel.findOneAndUpdate({_id: package.roomId}, {$pull: {invitation: package.userId}}, function(err) {
            if(err) {
                socket.emit('error', {message: 'DB-error on delete invite'});
                return false;
            }
            getUserInvite(socket, package.userId, function(invites) {
                Array.prototype.forEach.call(connectionMap.clientToSockets[package.userId], function(sock) {
                    io.to(sock).emit('invite', invites);
                });
            });
            getRoomUsers(io, socket, connectionMap, package);
        });
    }

};

module.exports.receiveMessage = function(io, package) {
    var messageModel = new MessageModel({
        date_time: Date.now(),
        user_id: package.cid,
        room_id: package.roomId,
        message: package.text
    });
    messageModel.save(function(err, message) {
        if(err) {
            console.log('Create default error' + err.message);
            return false;
        }else {
            package.dateTime = message.date_time;
            io.to(package.roomId).emit('message', package);
        }
    });
};

module.exports.getMessages = function(socket, package) {
    MessageModel.find({room_id: package.roomId}).sort({date_time: 'desc'}).limit(50)
        .populate('user_id', 'user_name logo_url').exec(function(err, result) {
        if(err) {
            console.log('Create default error' + err.message);
            return false;
        }
        var messages = [];
        if(result.length) {
            Array.prototype.forEach.call(result, function(item) {
                messages.push({
                    sender: item.user_id.user_name,
                    logoUrl: item.user_id.logo_url,
                    text: item.message,
                    dateTime: item.date_time,
                    roomId: item.room_id,
                    cid: item.user_id._id
                });
            });
        }
        socket.emit('message', messages);
    });
};