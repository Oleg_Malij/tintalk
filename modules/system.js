var os = require('os');

exports.networkSettings = function() {
    var port = 3000;
    var ip = null;
    var interfaces = os.networkInterfaces();
    var ipRegex = new RegExp('^\\d{2,3}\\.\\d{2,3}');
    for(var net in interfaces) {
        //console.log(interfaces[net]);
        Array.prototype.forEach.call(interfaces[net], function(i) {
            if(!i.internal && i.family === 'IPv4' && ipRegex.test(i.address)) {
                ip = i.address;
            }
        });
    }
    return {port: port, ip: ip};
}