var express = require('express');
var router = express.Router();
var system = require('../modules/system');

/* GET chat screen. */
router.get('/', function(req, res) {
    var connectionParams = system.networkSettings();
    res.render('chat', {
            chatServerAddress: 'http://' + connectionParams.ip + ':' + connectionParams.port + '/'
        });
});

module.exports = router;
