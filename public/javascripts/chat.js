angular.module('tinTalk', ['luegg.directives']);

angular.module('tinTalk').controller('tinTalkMainCtrl', ['$scope', 'Socket', 'Message', function($scope, Socket, Message) {
    $scope.cid = document.getElementById('tinTalkUniqueId').value;
    $scope.message = '';
    $scope.messageList = [];
    $scope.hideChatFlag = true;
    $scope.expandChatFlag = false;
    $scope.tinChat = {};
    $scope.tinChat.errorMessage = null;
    $scope.tinChat.rooms = []; // room models + user list, except messages
    $scope.tinChat.invites = []; // invites to other rooms
    $scope.tinChat.messages = {}; // roomId: [messages]
    $scope.activeRoom = null;
    $scope.findUserInput = '';
    $scope.showRoomBlock = true;
    $scope.showInviteBlock = false;

    var autoLoadRoom = [];
    $scope.$watch('activeRoom', function(newVal) {
        if(newVal) {
            if($scope.showInviteBlock) {
                exitRoomBlock();
            }
            if(autoLoadRoom.indexOf(newVal.id) === -1) {
                $scope.tinChat.messages[newVal.id] = [];
                $scope.messageList = $scope.tinChat.messages[newVal.id];
                autoLoadRoom.push(newVal.id);
                Message.getMessages(newVal.id);
            }else {
                $scope.messageList = $scope.tinChat.messages[newVal.id];
            }
        }
    });

    $scope.sendMessage = function(){
        if($scope.message.replace(/^\s+|\s+$/, '') === '') {
            return false;
        }
        var package = {
            text: $scope.message,
            sender: document.getElementById('tinTalkSender').value,
            logoUrl: document.getElementById('tinTalkSenderLogo').value,
            roomId: $scope.activeRoom.id,
            cid: $scope.cid
        };
        Socket.emit('message', package);
        $scope.message = '';
    };

    $scope.hideChat = function() {
        $scope.hideChatFlag = !$scope.hideChatFlag;
        $scope.expandChatFlag = false;
    };

    $scope.expandChat = function() {
        $scope.expandChatFlag = !$scope.expandChatFlag;
    };

    $scope.createRoom = function() {
        var roomName = $scope.tinChat.newRoomName;
        if(typeof roomName === 'undefined' || roomName.replace(/^\s+|\s+$/, '') === '') {
            return false;
        }
        var package = {
            cid: document.getElementById('tinTalkUniqueId').value,
            roomName: roomName.replace(/^\s+|\s+$/, ''),
            mode: 'create'
        };
        Socket.emit('room', package);
        $scope.tinChat.newRoomName = '';
        $scope.showCreateRoom = false;
    };

    $scope.cancelNewRoom = function() {
        $scope.showCreateRoom = false;
        $scope.tinChat.newRoomName = '';
    };

    $scope.inviteToRoom = function() {
        $scope.showRoomBlock = false;
        $scope.showInviteBlock = true;
        var package = {
            cid: $scope.cid,
            roomId: $scope.activeRoom.id
        };
        // Get a list of invited users for current room
        Socket.emit('roomUsers', package);
    };

    $scope.closeRoomBlock = function() {
        exitRoomBlock();
    };

    $scope.$watch('findUserInput', function(newVal) {
        if(newVal) {
            if ($scope.findUserInput.replace(/^\s+|\s+$/, '') !== '') {
                var package = {
                    cid: $scope.cid,
                    roomId: $scope.activeRoom.id,
                    searchPart: $scope.findUserInput
                };
                Socket.emit('findUser', package);
            }
        }else {
            $scope.foundUsers = [];
        }
    });

    Socket.on('findUser', function(package) {
        if(package.length) {
            $scope.foundUsers = package;
        }else {
            $scope.foundUsers = [{cid: null, userName: 'No users found by request: ' + '"' + $scope.findUserInput + '"'}];
        }
    });

    $scope.removeUserFromRoom = function(user, roomId) {
        var package = {
            cid: $scope.cid,
            userId: user.id,
            roomId: roomId,
            inviteStatus: user.inviteStatus
        };
        Socket.emit('removeRoomUser', package);
    };

    $scope.sendInvitation = function(user) {
        var package = {
            cid: $scope.cid,
            roomId: $scope.activeRoom.id,
            userId: user.cid,
            mode: 'create'
        };
        Socket.emit('invitation', package);
        exitRoomBlock();
    };

    $scope.acceptRejectInvite = function(invite, accept) {
        var package = {
            cid: $scope.cid,
            roomId: invite.id
        };
        package.mode = 'apply';
        if(!accept) {
            package.mode = 'delete';
        }
        Socket.emit('invitation', package);
    };

    function exitRoomBlock() {
        $scope.foundUsers = [];
        $scope.findUserInput = '';
        $scope.showRoomBlock = true;
        $scope.showInviteBlock = false;
    }

    $scope.deleteRoom = function(room, event) {
        event.stopPropagation();
        var package = room;
        package.mode = 'delete';
        package.cid = document.getElementById('tinTalkUniqueId').value;
        Socket.emit('room', package);
    };

    $scope.leaveRoom = function(room, event) {
        event.stopPropagation();
        var package = room;
        package.cid = $scope.cid;
        package.mode = 'delete';
        Socket.emit('room', package);
    };

    Socket.on('message', function(package){
        if(package.push) {
            if(package.length) {
                package.forEach(function (message) {
                    //$scope.messageList.push(message);
                    receiveMessage(message);
                });
            }
        }else {
            // Use for check when activeRoom is not equal to message room, and notify room about new incoming
            if(package.roomId !== $scope.activeRoom.id) {
                //$scope.messageList.push(package);
            }
            receiveMessage(package);
        }
    });

    Socket.on('room', function(package) {
        if(package.rooms.length) {
            $scope.tinChat.rooms = package.rooms;
        }
    });

    Socket.on('invite', function(package) {
        $scope.tinChat.invites = package;
    });

    Socket.on('roomUsers', function(package) {
        Array.prototype.forEach.call($scope.tinChat.rooms, function(roomItem) {
            if(roomItem.id === package.roomId) {
                roomItem.users = package.users;
                return false;
            }
        });
    });

    Socket.on('userOnline', function(res) {
        $scope.tinChat.online = res.online;
    });

    Socket.on('error', function(res) {
        alert(res.message);
    });

    function receiveMessage(message) {
        var roomId = message.roomId;
        delete message.roomId;
        if(!$scope.tinChat.messages[roomId]) {
            $scope.tinChat.messages[roomId] = [];
        }
        $scope.tinChat.messages[roomId].push(message);
    }

}]);

angular.module('tinTalk').directive('setActiveRoom', function(Message) {
    var room = null;
    return {
        restrict: 'A',
        scope: false,
        controller: function($scope, $element, $attrs) {
            if($scope.$first) {
                room = $scope.room;
            }
            if($scope.$parent.activeRoom && $scope.$parent.activeRoom.id === $scope.room.id) {
                room = $scope.room;
            }
            if($scope.$last) {
                $scope.$parent.activeRoom = room;
            }

            $scope.setActiveRoom = function(room) {
                $scope.$parent.activeRoom = room;
                /*if(!$scope.$parent.tinChat.messages[room.id]) {
                    Message.getMessages(room.id);
                }*/
            };
        }
    };
});

angular.module('tinTalk').service('Message', ['Socket',function(Socket) {
    return {
        getMessages: function getMessages(roomId) {
            var package = {roomId: roomId};
            package.cid = document.getElementById('tinTalkUniqueId').value;
            Socket.emit('getMessages', package);
        },
        invite: function invite(roomId, user) {

        }
    };
}]);

angular.module('tinTalk').factory('Socket',['$rootScope', function($rootScope) {
    var nodeAppUrl = document.getElementById('chatServerAddress').value;
    var handshake = 'cid=' + document.getElementById('tinTalkUniqueId').value +
                    '&sender=' + document.getElementById('tinTalkSender').value +
                    '&logoUrl=' + document.getElementById('tinTalkSenderLogo').value;
    var socket = io.connect(nodeAppUrl, {query: handshake});
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById('tinTalkApp'),['tinTalk']);
});