var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var io = require('socket.io')(http);
var promise = require('promise');
var jade = require('jade');
var bodyParser = require('body-parser');
var system = require('./modules/system');
var mongoModule = require('./modules/mongodb');

var connectionMap = {
    clientToSockets: {}, // sockets collection for connected client
    socketToClientId: {} // socket to client ID relation
};
var DEFAULT_ROOMS = ['Public'];
var indexRoute = require('./routes/index');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Allow cross origin to get fonts/icons
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
});
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRoute);

io.on('connection', function(socket) {
    var handshake = socket.handshake.query;
    mongoModule.getUserById(handshake.cid, function(user) {
        if(!connectionMap.clientToSockets[handshake.cid]) {
            connectionMap.clientToSockets[handshake.cid] = [];
        }
        if(connectionMap.clientToSockets[handshake.cid].indexOf(socket.id) === -1) {
            connectionMap.clientToSockets[handshake.cid].push(socket.id);
        }
        connectionMap.socketToClientId[socket.id] = handshake.cid;
        if(!user) {
            mongoModule.createUser(handshake, function(user) {
                mongoModule.getRooms(user, function(rooms) {
                    joinExistingRooms(rooms, socket);
                    io.emit('userOnline', {online: Object.keys(connectionMap.clientToSockets).length});
                    socket.emit('room', {rooms: rooms});
                });
            });
        }else {
            mongoModule.getRooms(user, function(rooms) {
                joinExistingRooms(rooms, socket);
                //console.log(io.sockets.adapter.rooms);
                io.emit('userOnline', {online: Object.keys(connectionMap.clientToSockets).length});
                socket.emit('room', {rooms: rooms});
                mongoModule.getUserInvite(socket, handshake.cid, function(invites) {
                    socket.emit('invite', invites);
                });
            });
        }
    });

    socket.on('message', function(package) {
        mongoModule.receiveMessage(io, package);
    });

    socket.on('getMessages', function(package) {
        mongoModule.getMessages(socket, package);
    });

    socket.on('room', function(package) {
        if(package.mode) {
            mongoModule[package.mode + 'Room'].apply({},[io, socket, connectionMap, package]);
        }
    });

    socket.on('roomUsers', function(package) {
        mongoModule.getRoomUsers(io, socket, connectionMap, package);
    });

    socket.on('removeRoomUser', function(package) {
        mongoModule.deleteUserFromRoom(io, socket, connectionMap, package);
    });

    socket.on('findUser', function(package){
        mongoModule.getUserByName(package).then(function(users) {
            socket.emit('findUser', users);
        });
    });

    socket.on('invitation', function(package) {
        if(package.mode) {
            mongoModule[package.mode + 'Invite'].apply({}, [io, socket, connectionMap, package]);
        }
    });

    socket.on('disconnect', function() {
        // No need to handle leave room action
        if(connectionMap.socketToClientId[socket.id]) {
            var cid = connectionMap.socketToClientId[socket.id];
            var socketIndex = connectionMap.clientToSockets[cid].indexOf(socket.id);
            if(socketIndex !== -1) {
                connectionMap.clientToSockets[cid].splice(socketIndex, 1);
                if(!connectionMap.clientToSockets[cid].length) {
                    delete connectionMap.clientToSockets[cid];
                }
            }
            delete connectionMap.socketToClientId[socket.id];
        }
        io.emit('userOnline', {online: Object.keys(connectionMap.clientToSockets).length});
    });

    function joinExistingRooms(rooms, socket) {
        if(rooms.length) {
            Array.prototype.forEach.call(rooms, function(room) {
                socket.join(room.id);
            });
        }
    }
});

function init() {
    var connectionParams = system.networkSettings();
    http.listen(connectionParams.port, function() {
        console.log('listening on - ' + connectionParams.ip + ':' + connectionParams.port);
    });
    mongoModule.createDefaultRooms(DEFAULT_ROOMS);
}

init();

module.exports = app;